console.log('<===NO.1===>')
//Release 0
class Animal {
    constructor(nama){
    	this.name = nama
    	this.legs = 4
    	this.cold_blooded = false
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log(' ')
//release 2
class Ape extends Animal{
	constructor(nama){
		super(nama);
		this.legs = 2
	}
	yell(){
		return console.log('Auooo')
	}
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"


class Frog extends Animal{
	constructor(nama){
		super(nama);
		this.cold_blooded = true
	}
	jump(){
		return console.log('hop hop')
	}
}
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 
console.log('<===NO.2===>')
class Clock{
    constructor({template}){
        this.template = template
        this.timer;
    }

    start(){
        this.timer = setInterval(() => this.render(), 1000)
        this.render()
    }
    render(){
        let date = new Date()
        var hours = date.getHours()
        if (hours < 10) hours = '0' + hours;
        var mins = date.getMinutes()
        if (mins < 10) mins = '0' + mins;
        var secs = date.getSeconds()
        if (secs < 10) secs = '0' + secs;
        
        var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output)
    }
    stop(){
        clearInterval(this.timer)
    }
}
var clock = new Clock({template:'h:m:s'})
console.log(clock.start())
