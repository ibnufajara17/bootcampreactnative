import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Axios from 'axios';
//import Telegram from './Tugas/Tugas12/Telegram';
import Login from './Tugas/Tugas13/loginScreen';
import Register from './Tugas/Tugas13/registerScreen';
import About from './Tugas/Tugas13/aboutScreen';
import RestAPI from './Tugas/Tugas14/restAPI';

export default function App() {
  return (
    //<Telegram />
    //<Login />
    //<Register />
    //<About />
    <RestAPI />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
