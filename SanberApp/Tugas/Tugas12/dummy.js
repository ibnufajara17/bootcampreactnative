const Data = [
    {
        id : "1",
        image: require("./asset/profile1.png"),
        name:'Achmad Hilmy' ,
        message:'Hello Achmad Hilmy',
        time:'12.00',
        totalMessage:'2'
    },
    {
        id : "2",
        image: require("./asset/profile2.png"),
        name:'Deddy Corbuizer' ,
        message:'Hello Ded',
        time:'12.00',
        totalMessage:'4'
    },
    {
        id : "3",
        image: require("./asset/profile3.png"),
        name:'Bobon Santoso' ,
        message:'Hello Bon',
        time:'12.00',
        totalMessage:'1'
    }
]

export { Data }