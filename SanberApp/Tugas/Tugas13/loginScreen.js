import React from 'react'
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, SafeAreaView } from 'react-native'

const LoginScreen = () => {
    return (
        <View style={styles.container}>
            <Image style={styles.logo} source={require('./asset/logo.png')}/>
            <Text style={{fontSize:24, color:"#003366", paddingTop:60}}>LOGIN</Text>

            <View>
                <Text style={{paddingTop:10, color:'#173366'}}>username</Text>
                <TextInput style={styles.borderInput}/>
                <Text style={{color:'#173366'}}>password</Text>
                <TextInput style={styles.borderInput}/>
            </View>
            <SafeAreaView style={{alignItems:'center', marginVertical:25}}>
                <TouchableOpacity>
                    <View style={{paddingVertical:2, paddingHorizontal:4, backgroundColor:"#3EC6FF",borderRadius:10}}>
                        <Text style={{color:'white', marginHorizontal:20, marginVertical:6, fontWeight:'bold', fontSize:18, }}>Masuk</Text>
                    </View>
                </TouchableOpacity>
                <Text style={{marginVertical:20, color:"#3EC6FF", fontWeight:'bold'}}>Atau</Text>
                <TouchableOpacity>
                    <View style={{paddingVertical:2, paddingHorizontal:4,borderRadius:10, borderWidth:1, backgroundColor:"#173366" }}>
                        <Text style={{color:"white", marginHorizontal:20, marginVertical:6, fontWeight:'bold', fontSize:18, }}>Daftar</Text>
                    </View>
                </TouchableOpacity>
            </SafeAreaView>
            

            
        </View>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    logo:{
        height:100,
        width:300,
    },
    borderInput:{
        borderWidth:1, 
        borderColor:"#173366",
        width:300,
        height:40
    }
})
