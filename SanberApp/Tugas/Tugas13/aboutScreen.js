import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'; 

const AboutScreen = () => {
    return (
        <View style={styles.container}>
            <Text style={{marginVertical:20, fontWeight:'bold', fontSize:22, color:'#40668C'}}>Tentang Saya</Text>
            <View style={styles.avatarWrap}>
                <MaterialCommunityIcons style={styles.avatar} name="account" size={24} color="black" />
            </View>

            <Text style={{fontSize:20, marginVertical:10, fontWeight:'bold', color:'#40668C'}}>Ibnu Fajar Anwarudin</Text>
            <Text style={{marginBottom:10, fontSize:20, color:'#64C6FE'}}>IT Support</Text>

            <View style={{width:300, height:120, backgroundColor:"#EFEFEF", paddingHorizontal:15}}>
                <Text style={styles.titleInformation}>Portofolio</Text>
                <View style={{flexDirection:'row', justifyContent:'space-evenly', marginTop:15}}>
                    <View style={{alignItems:'center'}}>
                        <MaterialCommunityIcons  name="gitlab" size={24} color="#3EC6FF" />
                        <Text style={styles.account_logo} >@ibnufajara17</Text>
                    </View>
                    <View style={{alignItems:'center'}}>
                        <MaterialCommunityIcons  name="github" size={24} color="#3EC6FF" />
                        <Text style={styles.account_logo}>@ibnufajara17</Text>
                    </View>
                </View>
            </View>
            <View style={{marginTop:10 ,width:300, height:150, backgroundColor:"#EFEFEF", alignItems:'center', paddingHorizontal:15}}>
                        <View style={{width:270}}>
                        <Text style={styles.titleInformation}>Contact</Text>
                        </View>
                     
                     <View style={{flexDirection:'row', alignItems:'center'}}>
                        <MaterialCommunityIcons  name="whatsapp" size={30} color="#3EC6FF" />
                        <Text style={styles.account_logo}>085334502285</Text>
                     </View>
                     <View style={{flexDirection:'row', alignItems:'center'}} >
                        <MaterialCommunityIcons  name="instagram" size={30} color="#3EC6FF" />
                        <Text style={styles.account_logo}>@ibnufajar_17</Text>
                     </View>
                     <View style={{flexDirection:'row', alignItems:'center'}}>
                        <MaterialCommunityIcons  name="telegram" size={30} color="#3EC6FF" />
                        <Text style={styles.account_logo}>@ibnufajar_17</Text>
                     </View>
            </View>
        </View>
    )
}

export default AboutScreen

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    avatarWrap:{
        width:180,
        height:180,
        borderRadius:90,
        backgroundColor:"#EFEFEF",
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'
    },
    avatar:{
        color:"#CACACA",
        fontSize:160,
    },
    titleInformation:{
        color:'#003366',
        fontSize:18, 
        margin:5, 
        borderBottomWidth:1,
        borderColor:'#003366'
    },
    account_logo:{
        marginLeft:10,
        color:'#003366'
    }
})
