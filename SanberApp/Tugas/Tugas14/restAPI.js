import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    FlatList,
    TouchableOpacity,
    Alert
} from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import Axios from 'axios';


const Item = ({ title, value, onPress, onDelete }) =>{
    return(
        <View style={StyleSheet.itemContainer}>
           <MaterialCommunityIcons style={styles.avatar} name="account" size={16} color="black" />
           <TouchableOpacity onPress={onPress}>
                  <Text style={styles.destTitle}>{title}</Text>
            </TouchableOpacity>
            <Text style={styles.descValue}></Text>
           </View>
           <TouchableOpacity onPress={onDelete}>
                <MaterialCommunityIcons style={styles.avatar} name="delete" size={16} color="red" />
           </TouchableOpacity>
        </View>
    )
}

const API = () => {
    const [title, setTitle] = useState("");
    const [value, setValue] = useState ("");
    const [detail,setDetail] = useState([]);
    const [submit, setSubmit] = useState("Simpan");
    const [selected, setSelected] = useState([])

    useEffect(()=> {
        getData();
    }, [])

    const submit = () =>{
        const data = {
            title,
            value,
        }
        if (submit === 'Simpan') {

            Axios.post('https://achmadhilmy-sanbercode.my.id/api/v1/news', data)
                .then(res => {
                    console.log('res:', res);
                    setTitle("");
                    setValue("");
                    getDataNews();
                })
        } else if (button === 'Update') {
            Axios.put(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${selected.id}`, data)
                .then(res => {
                    console.log('res Update:', res);
                    setTitle("");
                    setValue("");
                    getDataNews();
                    setSubmit("Simpan");

                })
        }
    }
    const getDataNews = () => {
        Axios.get('https://achmadhilmy-sanbercode.my.id/api/v1/news')
            .then((res) => {
                setDetail(res.data.data)
            })
            .catch((err) => {
                alert(err)
            })
    }
    const selectItem = (item) => {
        console.log('Selected item:', item);
        setSelected(item);
        setTitle(item.title);
        setValue(item.value);
        setButton("Update");
    }
    const deleteItem = (item) => {
        Axios.delete(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${item.id}`)
            .then(res => {
                console.log('res: ', res)
                getDataNews()
            }).catch(err => {
                console.log('error: ', err)
            })
    }
    return(
        <View style={styles.container}>
            <Text style={styles.textTitle}>API Server</Text>
            <Text>Masukan Berita Hari ini</Text>
            <TextInput placeholder="Judul" style={styles.input} value={title} onChangeText={(value) => setTitle(value)}/>
            <TextInput placeholder="Isi" style={styles.input} value={value} onChangeText={(value) => setValue(value)}/>
            <Button title="Simpan" onPress={submit}/>
            <View style={styles.line} />
            {
                detail.map((item) => {
                    return <Item key={item.id} title={item.title} value={item.value} onPress={() => selectItem(item)} onDelete={() => deleteItem(item)} />
                })
            }
        </View>
    )
}
export default restAPI

const styles = StyleSheet.create({
    container: {
        padding: 15
    },
    textTitle: {
        textAlign: 'center',
        marginBottom: 20
    },
    line: {
        height: 2, 
        backgroundColor: 'black', 
        marginVertical: 20
    },
    input: {
        borderWidth: 1, 
        marginBottom: 12,
        borderRadius: 25, 
        paddingHorizontal: 12
    }
    itemContainer:{
        flexDirection: 'row',
        justifyContent:'center',
        alignItems:'center'
    },
    avatar:{
        color:"#CACACA",
    },
    destTitle:{
        fontSize: 18,
        color: 'black'
    },
    destValue:{
        fontSize: 18,
        color: 'black'
    }
})