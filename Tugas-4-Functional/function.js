//FUNCTION
console.log("NO.1")
//Tulislah sebuah function dengan nama teriak() yang mengembalikan nilai “Halo Sanbers!” yang kemudian dapat ditampilkan di console
function suara(){
	return "Halo Sanbers!";
}
var teriak = suara();
console.log(teriak);

console.log("");

console.log("NO.2")
//Tulislah sebuah function dengan nama kalikan() yang mengembalikan hasil perkalian dua parameter yang di kirim.
var no1 = 12;
var no2 = 4;
function kalikan(no1, no2){
	return no1 * no2;
}
var hasilKali = kalikan(no1, no2);
console.log(hasilKali);

console.log("");

console.log("NO.3")
//Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: “Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!”
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

function introduce(name, age, address, hobby){
	return "Nama saya "+name+", umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+"!";
} 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);