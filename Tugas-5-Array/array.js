//FUNCTION
console.log("NO.1")
function range(startNum, finishNum) { 
	var rangeArr = []; 
	var rangeLength = Math.abs(startNum - finishNum) + 1; 
	if (startNum > finishNum) { 
		var rangeLength = startNum - finishNum + 1; 
		for (var i = 0; i < rangeLength; i++) { 
			rangeArr.push(startNum - i) 
		} 
	} else if (startNum < finishNum) { 
		var rangeLength = finishNum - startNum + 1; 
		for (var i = 0; i < rangeLength; i++) { 
			rangeArr.push(startNum + i) 
		} 
	} else if (!startNum || !finishNum) { 
		return -1 
	} 
	return rangeArr 
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log(" ")
console.log("NO.2")
function rangeWithStep(startNum, finishNum, step){
	var rangeArrstep = [];

	if (startNum > finishNum){
		var currentNum = startNum;
		for (var i = 0; currentNum >= finishNum; i++) { 
			rangeArrstep.push(currentNum)
			currentNum -= step
		} 
	}else if (startNum < finishNum){
		var currentNum = startNum;
		for (var i = 0; currentNum <= finishNum; i++) { 
			rangeArrstep.push(currentNum)
			currentNum += step
		}
	} else if (!startNum || !finishNum || !step) { 
		return -1 
	} 
	return rangeArrstep 
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log(" ")
console.log("NO.3")
function sum(startNo, finishNo, step){
	var rangeSum = [];
	if((startNo, finishNo, step)){
		if(startNo < finishNo){
			var curNum = startNo;
			for( var i = 0; curNum <= finishNo; i++){
				rangeSum.push(curNum);
				curNum += step;
			}
		} else {
			var curNum = startNo;
			for( var i = 0; curNum >= finishNo; i++){
				rangeSum.push(curNum);
				curNum -= step;
			}
		}
		var sum = rangeSum.reduce((total,value)=> total + value);
		rangeSum = sum;
	} else if (startNo, finishNo){
		if(startNo < finishNo){
			for ( i = startNo; i <= finishNo;i++){
				rangeSum.push(i);
			}
			var sum = rangeSum.reduce((total,value)=> total + value);
			rangeSum = sum;
		}else{
			for( i = finishNo; i <= startNo; i++){
				rangeSum.push(i);
			}
			var sum = rangeSum.reduce((total,value)=> total + value);
			rangeSum = sum;
		}
	}else if (startNo >= 0){
		rangeSum = startNo;
	}else{
		rangeSum = 0;
	}
	return rangeSum;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

console.log(" ")
console.log("NO.4")

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
  ] ;

 function data_handling(length){
 	var arr_in = input;

 	if(typeof length == "string"){
 		console.log("Inputkan data secara angka");
 	}else if(length > input.length){
 		console.log('Inputkan data antara 1- ${arr_in.length}');
 	}else if(length <= 1){
 		console.log('Inputkan data antara 1- ${arr_in.length}');
 	}else if(length){
 		for ( i = 0;i < length; i++){
 			 console.log("Nomor Id : " + arr_in[i][0]);
		     console.log("Nama Lengkap : " + arr_in[i][1]);
		     console.log("TTL : " + arr_in[i][2] + ", " + arr_in[i][3]);
		     console.log("Hobi : " + arr_in[i][4]);
		     console.log("");
 		 }
  } else {
    console.log("Tidak di temukan");
  }
}
data_handling(4);

console.log(" ")
console.log("NO.5")

function balikKata (word){
    var length = word.length - 1
    var kata = ''
    for(i=length; i>=0; i--){
        kata += word[i]
    }
    return kata

}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


console.log(" ")
console.log("NO.6")
var input = [
  "0001",
  "Roman Alamsyah ",
  "Bandar Lampung",
  "21/05/1989",
  "Membaca",
];

function dataHandling2(input) {
    
  input.splice(-1)
  input.push("Pria", "SMA Internasional Metro")
  input.splice(1,1, 'Roman Alamsyah Elsharawy')
  input.splice(2,1, "Provinsi Bandar Lampung")
  console.log(input);
  var dataKelahiran = input[3];
  dmy = dataKelahiran.split("/");

  var bulan = parseInt(dmy[1]);

  switch (bulan) {
    case 01:
      console.log("January");
      break;
    case 02:
      console.log("February");
      break;
    case 03:
      console.log("Maret");
      break;
    case 04:
      console.log("April");
      break;
    case 05:
      console.log("Mei");
      break;
    case 06:
      console.log("Juni");
      break;
    case 07:
      console.log("Juli");
      break;
    case 08:
      console.log("Agustus");
      break;
    case 09:
      console.log("September");
      break;
    case 10:
      console.log("Oktober");
      break;
    case 11:
      console.log("November");
      break;
    case 12:
      console.log("Desember");
      break;
    default:
      console.log("bulan");
  }
  var copy_array = [...dmy]
  var newVar = copy_array.sort(function(a,b){return b-a})
  console.log(newVar)


  var slug = dmy.join('-')
  console.log(slug)

  var nama = input[1]
  console.log(nama.slice(0,15))
}

dataHandling2(input);

