console.log("<=== NO.1 ===>")
const golden = goldenFunction = () =>{
	console.log("this is golden!!")
}
golden()
console.log("<=== NO.2 ===>")
//2. Sederhanakan menjadi Object literal di ES6
const newFunction = literal = (firstName, lastName) =>{
  return {
    firstName,
    lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 
console.log("<=== NO.3 ===>")
//3.destructing
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
const {firstName, lastName, destination, occupation} = newObject;
console.log(firstName, lastName, destination, occupation);
console.log("<=== NO.4 ===>")
//4. Array Spending
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)
console.log("<=== NO.5 ===>")
//5.sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:
const planet = "earth"
const view = "glass"
const before = `Lorem  ${view} dolor sit amet consectetur adipiscing elit ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(before)