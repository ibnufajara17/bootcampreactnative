//NO.1 LOOPING WHILE
console.log("NO.1");
console.log("LOOPING PERTAMA");
var x = 2;
while( x < 21){
	console.log(x + ' - I Love Coding');
	x+=2;
}
console.log("LOOPING KEDUA");
var y = 20;
while( y > 1){
	console.log(y + ' - I will become a mobile developer');
	y-=2;
}
console.log(" ");
//NO.2 LOOPING FOR
console.log("NO.2");
for (z=1;z<=20;z++){
    if (z%2==0){
        console.log(z+" Berkualitas ");
    }else if (z%3==0){
        console.log(z+" I Love Coding ");
    }else{
    	console.log(z+" Santai ");
    }     
}
console.log(" ");
//NO.3 Membuat Persegi Panjang #
console.log("NO.3");
var v=1;
while(v < 5){
	console.log("#########");
	v++;
}
console.log(" ");
//NO.4 Membuat Tangga
console.log("NO.4");
var spasi = "";
for(i=1; i<8; i++){
    spasi += '#'
    console.log(spasi)
}

console.log(" ");
//NO.5 Membuat Catur 8X8
console.log("NO.5");
var papan ="";
var uk = 8;
for(k=1; k <= uk ;k++){
	for(l=1; l <= uk;l++){
		if((k+l) % 2 == 0){
			papan += " ";
		}else{
			papan += "#";
		}
	}
	papan += "\n";
}
console.log(papan);
