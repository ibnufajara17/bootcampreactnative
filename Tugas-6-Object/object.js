//Soal No.1
console.log("<=== NO.1 ===>")
function arrayToObject(masuk){
	var now = new Date()
	var thisYear = now.getFullYear()
	var data = []
	var y = masuk.length;
	//var invalid = console.log("Invalid Birth Year");
	for(x = 0; x < y; x++){
		var input = masuk[x];
		var obj = [];
		obj.firstName = input[0];
		obj.lastName = input[1];
		obj.gender = input[2];
		if(input[3] < thisYear){
			obj.age = thisYear - input[3];
		}else if (input[3] > thisYear){
			obj.age = "Invalid Birth Year"
		}else{
			obj.age = "Invalid Birth Year"
		}
		data.push(obj);
	}
	for(x = 0; x < masuk.length; x++){
		console.log(`${input[0]} ${input[1]} : `, data[x])
	}
}
var people =  [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
console.log(" ")
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
//Soal No.2
console.log("<=== NO.2 ===>")
function shoppingTime(memberId, money){
	var invalid = ''
	var objShop = {}
	if(!memberId){
		invalid = 'Mohon maaf, toko X hanya berlaku untuk member saja'
		return invalid
	}else if( money <= 49999){
		invalid = 'Mohon maaf, uang tidak cukup'
		return invalid
	}else if( memberId && money ){
		var saldo = money
		var listPurchased = [];
		if( saldo <= 49999 ){
			invalid = 'Mohon maaf, uang tidak cukup'
			return invalid
		}
		for(z = 0; z <= 99999; z++){
			if(saldo >= 1500000){
				listPurchased.push('Sepatu Stacattu');
				saldo -= 1500000;
			}else if(saldo >= 500000){
				listPurchased.push('Baju Zoro');
				saldo -= 500000;
			}else if(saldo >= 250000){
				listPurchased.push('Baju H&N');
				saldo -= 250000;
			}else if(saldo >= 175000){
				listPurchased.push('Sweater Uniklooh');
				saldo -= 175000;
			}else if(saldo >= 50000){
				listPurchased.push('Casing Handphone Sungsang');
				saldo -= 50000;
			}
		}
		objShop.memberId = memberId
		objShop.money = money
		objShop.listPurchased = listPurchased
		objShop.changeMoney = saldo
	}
	return objShop
}
console.log(shoppingTime('324193hDew2', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//Soal No.3
console.log("<=== NO.3 ===>")
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var harga = 2000;
    var total = []
    var l = arrPenumpang.length
    if(l === 0){
    	return arrPenumpang;
    }
    for(k = 0;k < l;k++){
    	var penumpang = arrPenumpang[k];
    	var objAngkot = {};
    	objAngkot.penumpang = penumpang[0];
    	objAngkot.naikDari = penumpang[1];
    	objAngkot.tujuan = penumpang[2];
    	objAngkot.bayar = harga * (rute.indexOf(objAngkot.tujuan)-rute.indexOf(objAngkot.naikDari));
    	total.push(objAngkot);
    }
    return total;
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']])); 
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
console.log(naikAngkot([])); //[]

