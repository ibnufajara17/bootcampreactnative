//B. Tugas Conditional
// --IF ELSE

//1. Output ke 1
var nama = "";
var peran = "";


if(nama == "Mas Pur"){
    if(peran == "Guard"){
    	console.log('Selamat datang di Dunia Werewolf, ' + nama);
		console.log('Halo ' + peran + ' ' + nama +', kamu akan membantu melindungi temanmu dari serangan werewolf.');
	}else if(peran == "Penyihir"){
		console.log('Selamat datang di Dunia Werewolf, ' + nama);
		console.log('Halo ' + peran + ' ' + nama +', kamu dapat melihat siapa yang menjadi werewolf!');
	}
	else if(peran == "Warewolf"){
		console.log('Selamat datang di Dunia Werewolf, ' + nama);
		console.log('Halo ' + peran + ' ' + nama +', Kamu akan memakan mangsa setiap malam!');
	}
	else if(peran == ""){
		console.log('Halo '+ nama +', Pilih peranmu untuk memulai game!');	
	}
}else{
	console.log("Nama harus di isi!");
}


//2. Output ke 2
var nama2 = "Mas Pur";
var peran2 = "";

if(nama2 == "Mas Pur"){
    if(peran2 == "Guard"){
    	console.log('Selamat datang di Dunia Werewolf, ' + nama2);
		console.log('Halo ' + peran2 + ' ' + nama2 +', kamu akan membantu melindungi temanmu dari serangan werewolf.');
	}else if(peran2 == "Penyihir"){
		console.log('Selamat datang di Dunia Werewolf, ' + nama2);
		console.log('Halo ' + peran2 + ' ' + nama2 +', kamu dapat melihat siapa yang menjadi werewolf!');
	}
	else if(peran2 == "Warewolf"){
		console.log('Selamat datang di Dunia Werewolf, ' + nama2);
		console.log('Halo ' + peran2 + ' ' + nama2 +', Kamu akan memakan mangsa setiap malam!');
	}
	else if(peran2 == ""){
		console.log('Halo '+ nama2 +', Pilih peranmu untuk memulai game!');	
	}
}else{
	console.log("Nama harus di isi!");
}


//3. Output ke 3
var nama3 = "Mas Pur";
var peran3 = "Guard";

if(nama3 == "Mas Pur"){
    if(peran3 == "Guard"){
    	console.log('Selamat datang di Dunia Werewolf, ' + nama3);
		console.log('Halo ' + peran3 + ' ' + nama3 +', kamu akan membantu melindungi temanmu dari serangan werewolf.');
	}else if(peran3 == "Penyihir"){
		console.log('Selamat datang di Dunia Werewolf, ' + nama3);
		console.log('Halo ' + peran3 + ' ' + nama3 +', kamu dapat melihat siapa yang menjadi werewolf!');
	}
	else if(peran3 == "Warewolf"){
		console.log('Selamat datang di Dunia Werewolf, ' + nama3);
		console.log('Halo ' + peran3 + ' ' + nama3 +', Kamu akan memakan mangsa setiap malam!');
	}
	else if(peran3 == ""){
		console.log('Halo '+ nama3 +', Pilih peranmu untuk memulai game!');	
	}
}else{
	console.log("Nama harus di isi!");
}

//4. Output ke 4
var nama4 = "Gery";
var peran4 = "Warewolf";

if(nama4 == "Gery"){
    if(peran4 == "Guard"){
    	console.log('Selamat datang di Dunia Werewolf, ' + nama4);
		console.log('Halo ' + peran4 + ' ' + nama4 +', kamu akan membantu melindungi temanmu dari serangan werewolf.');
	}else if(peran4 == "Penyihir"){
		console.log('Selamat datang di Dunia Werewolf, ' + nama4);
		console.log('Halo ' + peran4 + ' ' + nama4 +', kamu dapat melihat siapa yang menjadi werewolf!');
	}
	else if(peran4 == "Warewolf"){
		console.log('Selamat datang di Dunia Werewolf, ' + nama4);
		console.log('Halo ' + peran4 + ' ' + nama4 +', Kamu akan memakan mangsa setiap malam!');
	}
	else if(peran4 == "" && peran4 != "Gery"){
		console.log('Halo '+ nama4 +', Pilih peranmu untuk memulai game!');	
	}
}
else{
	console.log("Nama harus di isi! ataupun sesuai");
}


//5. Output ke 5
var nama5 = "Santi";
var peran5 = "Penyihir";

if(nama5 == "Santi"){
    if(peran5 == "Guard"){
    	console.log('Selamat datang di Dunia Werewolf, ' + nama5);
		console.log('Halo ' + peran5 + ' ' + nama5 +', kamu akan membantu melindungi temanmu dari serangan werewolf.');
	}else if(peran5 == "Penyihir"){
		console.log('Selamat datang di Dunia Werewolf, ' + nama5);
		console.log('Halo ' + peran5 + ' ' + nama5 +', kamu dapat melihat siapa yang menjadi werewolf!');
	}
	else if(peran5 == "Warewolf"){
		console.log('Selamat datang di Dunia Werewolf, ' + nama5);
		console.log('Halo ' + peran5 + ' ' + nama5 +', Kamu akan memakan mangsa setiap malam!');
	}
	else if(peran5 == "" && peran5 != "Gery"){
		console.log('Halo '+ nama5 +', Pilih peranmu untuk memulai game!');	
	}
}
else{
	console.log("Nama harus di isi! ataupun sesuai");
}


//--- Swich Case
var hari =1;
var bulan =5;
var tahun =1945;

switch(bulan){
  case bulan = 1:
    bulan = "January";
    break;
  case bulan =2:
    bulan = "February";
    break;
  case bulan =3:
    bulan = "Maret";
    break;
  case bulan =4:
    bulan = "April";
    break;
  case bulan =5:
    bulan = "Mei";
    break;
  case bulan =6:
    bulan = "Juni";
    break;
  case bulan =7:
    bulan = "Juli";
    break;
  case bulan =8:
    bulan = "Agustus";
    break;
  case bulan =9:
    bulan = "September";
    break;
  case bulan =10:
    bulan = "Oktober";
    break;
  case bulan =11:
    bulan = "November";
    break;
  case bulan =12:
    bulan = "Desember";
    break;
  default:
    bulan = "Bulan";
}

console.log(hari+" "+bulan+" "+tahun);